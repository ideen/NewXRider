﻿using UnityEngine;
using Revy.Framework;

namespace MyNamespace
{
	public class #ClassName# : FComponent, I#ClassName#
	{
		#region Fields
        #endregion Fields

		#region Properties
        #endregion Properties

        #region Public Methods
        #endregion Public Methods

        #region FComponent's Callbacks       
        #endregion FComponent's Callbacks

        #region Helpers     
        #endregion Helpers
	}
}