﻿using UnityEditor;
using UnityEngine;
using Revy.Framework;

namespace Revy.Framework.Editor
{
    /// <summary>
    /// Helper class for instantiating ScriptableObjects.
    /// </summary>
    public class ScriptableObjectFactory
    {
        [MenuItem("Revy/Create/ScriptableObject")]
        public static void Create()
        {
            //Get all classes derived from ScriptableObject
            var allScriptableObjects = CUtilities.GetAllDerivedTypes(typeof(ScriptableObject));

            //Show the selection window.
            var window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
            window.ShowPopup();

            window.Types = allScriptableObjects;
        }
    } 
}