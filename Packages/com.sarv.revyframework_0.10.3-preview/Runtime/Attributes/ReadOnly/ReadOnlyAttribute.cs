﻿using UnityEngine;
namespace Revy.Framework
{
    /// <summary>
    /// Use this attribute to make read-only property in inspector.
    /// </summary>
    public class ReadOnlyAttribute : PropertyAttribute
    {

    }
}