﻿/*
 * Author: Mohammad Hasan Bigdeli
 * Creation Date: 1 / 27 / 2018
 */

namespace Revy.Framework.Localization
{
    public enum ELanguage
    {
        ENG,
        FA,
    }
}