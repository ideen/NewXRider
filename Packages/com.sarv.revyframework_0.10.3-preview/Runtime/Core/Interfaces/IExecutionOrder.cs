﻿namespace Revy.Framework
{
    public interface IExecutionOrder
    {
        int ExecutionOrder { get; }
    }
}