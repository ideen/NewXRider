﻿using System.Security.Authentication.ExtendedProtection;

namespace Revy.Framework
{
    public sealed partial class PersistentSubCategories
    {
        public static readonly string MANAGERS = "Managers";

        public static readonly string UTILITIES = "Utilities";

        public static readonly string UI = "UI";

        public static readonly string AUDIO = "Audio";

        public static readonly string OBJECTSPOOL = "PoolSystem";

        public static readonly string GameManager = "GameManager";

        public static readonly string SUBSYSTEMS = "Subsystems";

        public static readonly string OTHER = "Other";
    }
}