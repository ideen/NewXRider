using UnityEngine;
using Revy.Framework;

namespace Xrider.Motor
{
	public class MotorController : FComponent, IMotorController
	{
		#region Fields
        #endregion Fields

		#region Properties
        #endregion Properties

        #region Public Methods
        #endregion Public Methods

        #region FComponent's Callbacks       
        #endregion FComponent's Callbacks

        #region Helpers     
        #endregion Helpers
	}
}