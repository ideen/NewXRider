using UnityEngine;
using Revy.Framework;

namespace XRider.Managers
{
	public class InputController : FComponent, IInputController
	{
		#region Fields
        #endregion Fields

		#region Properties
        #endregion Properties

        #region Public Methods
        #endregion Public Methods

        #region FComponent's Callbacks       
        #endregion FComponent's Callbacks

        #region Helpers     
        #endregion Helpers
	}
}