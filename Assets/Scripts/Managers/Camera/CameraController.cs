using UnityEngine;
using Revy.Framework;

namespace XRider.Managers
{
	public class CameraController : FComponent, ICameraController
	{
		#region Fields
        #endregion Fields

		#region Properties
        #endregion Properties

        #region Public Methods
        #endregion Public Methods

        #region FComponent's Callbacks       
        #endregion FComponent's Callbacks

        #region Helpers     
        #endregion Helpers
	}
}